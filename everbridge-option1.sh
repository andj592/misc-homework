#!/bin/bash

find . -type f -name "/var/www/*.html" -exec sed -i -r "s@(\+1|1)?[[:space:]]?\(?[0-9]{3}\)?([[:space:]]|\.|-)?([0-9]|[A-Z]){3}([[:space:]]|\.|-)?([0-9]|[A-Z]){4}@202-221-1414@g" {} \;